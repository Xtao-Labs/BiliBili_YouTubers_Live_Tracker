from pyrogram import Client, filters
from pyrogram.types import Message, InlineKeyboardMarkup, InlineKeyboardButton
from ci import me
from plugins.info import vtuber_msg, gen_info_button
from defs.source import from_name_to_v, from_keyword_to_v
from defs.subs import add_to_subs, remove_from_subs

des = """
你好！{} 我是 [{}]({})，一个为 BiliBili Vtuber 用户打造的一体化机器人！
我可以帮助你获取 BiliBili Vtuber 的开播提醒和信息查询！

点击下面的帮助按钮来查看使用方法。
加入 [我的频道](https://t.me/DD_YTbs_Live_Tracker) 获取关于 BiliBili Vtuber 的所有开播提醒和公告！
"""
unsub_msg = """
<b>成功退订了</b> <code>{}</code> <b>的开播提醒！</b>
"""
not_sub_msg = """
<b>你好像没有订阅</b> <code>{}</code> <b>的开播提醒！</b>
"""
sub_msg = """
<b>成功订阅了</b> <code>{}</code> <b>的开播提醒！</b>
"""
already_sub_msg = """
<b>已经订阅过</b> <code>{}</code> <b>的开播提醒！</b>
"""
not_found_msg = """
<b>没有找到名为</b> <code>{}</code> <b>的 Vtuber！</b>
"""


def gen_help_button() -> InlineKeyboardMarkup:
    data_ = [[InlineKeyboardButton("📢 官方频道", url="https://t.me/DD_YTbs_Live_Tracker"),
              InlineKeyboardButton("💬 官方群组", url="https://t.me/Invite_Challenge_Bot?start=1"), ],
             [InlineKeyboardButton("❓ 阅读帮助", callback_data="help")],
             ]
    return InlineKeyboardMarkup(data_)


@Client.on_message(filters.incoming & filters.private &
                   filters.command(["start"]))
async def start_command(_: Client, message: Message):
    """
        回应消息
    """
    if len(message.command) == 1:
        await message.reply(des.format(message.from_user.mention(),
                                       me.name,
                                       f"https://t.me/{me.username}"),
                            reply_markup=gen_help_button(),
                            quote=True, )
    else:
        data = message.command[1]
        if data.startswith("un-"):
            # 退订
            name = data[3:]
            data = from_name_to_v(name)
            if data:
                success = remove_from_subs(message.from_user.id, data)
                if success:
                    await message.reply(unsub_msg.format(data.name), quote=True)
                else:
                    await message.reply(not_sub_msg.format(data.name), quote=True)
            else:
                await message.reply(not_found_msg.format(name), quote=True)
        elif data.startswith("info-"):
            data = data[5:]
            v = from_keyword_to_v(data)
            if v:
                await message.reply(
                    vtuber_msg.format(
                        v.name,
                        v.follower,
                        v.timeStr,
                        v.notice,
                    ),
                    reply_markup=gen_info_button(v),
                    quote=True,
                )
            else:
                await message.reply(not_found_msg.format(data), quote=True)
        else:
            # 订阅
            name = data
            data = from_name_to_v(data)
            if data:
                success = add_to_subs(message.from_user.id, data)
                if success:
                    await message.reply(sub_msg.format(data.name), quote=True)
                else:
                    await message.reply(already_sub_msg.format(data.name), quote=True)
            else:
                await message.reply(not_found_msg.format(name), quote=True)
