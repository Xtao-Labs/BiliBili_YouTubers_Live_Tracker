from pyrogram import Client, filters
from pyrogram.types import Message, InlineKeyboardMarkup, InlineKeyboardButton

from ci import sqlite, me
from defs.source import from_keyword_to_v
from defs.utils import Vtuber

info_help_msg = """
👩🏻‍💼 » /info <code>space_id|昵称|room_id</code> - 查询模块信息
     <code>/info 5659864</code>
     <code>/info 鹿野灸</code>
     <code>/info 2064239</code>
"""
vtuber_msg = """
<b>{}</b>

<b>粉丝数：</b><code>{}</code>
<b>更新时间：</b><code>{}</code>
<b>通知：</b>

<code>{}</code>

@DD_YTbs_Live_Tracker | @DD_YTbs_Bot
"""
not_found_msg = """
<b>没有找到名为</b> <code>{}</code> <b>的 Vtuber！</b>
"""


def gen_info_button(data: Vtuber) -> InlineKeyboardMarkup:
    msg_link = sqlite.get(str(data.room_id), {}).get("msg_link", "https://t.me/DD_YTbs_Live_Tracker")
    data_ = [[InlineKeyboardButton("详情", url=msg_link)], [
        InlineKeyboardButton("主页", url=data.space_link),
        InlineKeyboardButton("订阅",
                             url=f"https://t.me/{me.username}?start={data.room_id}"),
        InlineKeyboardButton("分享", switch_inline_query=data.name),
    ]]
    return InlineKeyboardMarkup(data_)


@Client.on_message(filters.incoming & filters.private &
                   filters.command(["info"]))
async def info_command(_: Client, message: Message):
    if len(message.command) == 1:
        await message.reply(info_help_msg, quote=True)
    else:
        data = " ".join(message.command[1:])
        v = from_keyword_to_v(data)
        if v:
            await message.reply(
                vtuber_msg.format(
                    v.name,
                    v.follower,
                    v.timeStr,
                    v.notice,
                ),
                reply_markup=gen_info_button(v),
                quote=True,
            )
        else:
            await message.reply(not_found_msg.format(data), quote=True)
