from pyrogram import Client, filters
from pyrogram.types import Message, InlineKeyboardMarkup, InlineKeyboardButton

help_msg = """
下面是我学会了的指令列表：

👩🏻‍💼 » /subscribe <code>space_id|昵称|room_id</code> - 订阅直播间
     <code>/subscribe 5659864</code>
     <code>/subscribe 鹿野灸</code>
     <code>/subscribe 2064239</code>

👩🏻‍💼 » /unsubscribe <code>space_id|昵称|room_id</code> - 取消订阅直播间

👩🏻‍💼 » /subscription - 列出您当前的订阅

👩🏻‍💼 » /info <code>space_id|昵称|room_id</code> - 查询主播信息
"""


@Client.on_message(filters.incoming & filters.private &
                   filters.command(["help"]))
async def help_command(_: Client, message: Message):
    await message.reply(
        help_msg,
        reply_markup=InlineKeyboardMarkup(
            [[InlineKeyboardButton("订阅", callback_data="subs")]]
        ),
        disable_web_page_preview=True,
        quote=True,
    )
