import traceback
from asyncio import sleep
from random import uniform

from pyrogram.errors import FloodWait, ButtonUrlInvalid
from pyrogram.types import Message

from ci import app, scheduler, channel_id, admin_id, sqlite, client
from pyrogram import Client, filters

from defs.format_time import strf_time, now_time
from defs.msg import gen_update_msg
from defs.source import update_data, compare, update_info
from defs.subs import send_to_subscribes


async def send_track_msg(track_msg, no_button=False) -> Message:
    button = None if no_button else track_msg.button
    if track_msg.img:
        return await app.send_photo(channel_id, track_msg.img, caption=track_msg.text,
                                    parse_mode="html",
                                    reply_markup=button)
    return await app.send_message(channel_id, track_msg.text,
                                  parse_mode="html",
                                  reply_markup=button)


@scheduler.scheduled_job("cron", minute="*/10", id="1")
async def run_every_10_minute():
    await update_data()
    need_update = compare()
    for i in need_update:
        data = (await client.get(f"https://api.tokyo.vtbs.moe/v1/room/{i.room_id}")).json()
        i.liveStartTime = data["live_time"]
        if i.liveStartTime == 0:
            i.liveStartTimeStr = now_time()
        else:
            i.liveStartTimeStr = strf_time(i.liveStartTime)
        track_msg = await gen_update_msg(i)
        msg = None
        try:
            msg = await send_track_msg(track_msg)
        except FloodWait as e:
            print(f"Send document flood - Sleep for {e.x} second(s)")
            await sleep(e.x + uniform(0.5, 1.0))
            msg = await send_track_msg(track_msg)
        except ButtonUrlInvalid:
            print(f"Send button error")
            msg = await send_track_msg(track_msg, no_button=True)
        except Exception as e:
            traceback.print_exc()
        await sleep(uniform(0.5, 2.0))
        data_ = sqlite.get(str(i.room_id), {"msg_link": ""})
        if msg:
            data_["msg_link"] = msg.link
        else:
            data_["msg_link"] = "https://t.me/DD_YTbs_Live_Tracker"
        sqlite[str(i.room_id)] = data_
        await send_to_subscribes(i)
        await sleep(uniform(0.5, 2.0))


@scheduler.scheduled_job("cron", hour="*/12", minute="0", id="0")
async def run_every_12_hour():
    await update_info()


@Client.on_message(filters.incoming & filters.private & filters.chat(admin_id) &
                   filters.command(["force_update", ]))
async def force_update(_: Client, __: Message):
    await run_every_12_hour()
    await run_every_10_minute()
