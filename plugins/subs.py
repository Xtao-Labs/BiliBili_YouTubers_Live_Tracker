from pyrogram import Client, filters
from pyrogram.types import Message

from defs.source import from_keyword_to_v
from defs.subs import gen_subs_msg, gen_back_button, add_to_subs, remove_from_subs
from plugins.start import sub_msg, not_found_msg, already_sub_msg, unsub_msg, not_sub_msg

sub_help_msg = """
👩🏻‍💼 » /subscribe <code>space_id|昵称|room_id</code> - 订阅直播间
     <code>/subscribe 5659864</code>
     <code>/subscribe 鹿野灸</code>
     <code>/subscribe 2064239</code>
"""
unsub_help_msg = """
👩🏻‍💼 » /unsubscribe <code>space_id|昵称|room_id</code> - 取消订阅直播间
     <code>/unsubscribe 5659864</code>
     <code>/unsubscribe 鹿野灸</code>
     <code>/unsubscribe 2064239</code>
"""


@Client.on_message(filters.incoming & filters.private &
                   filters.command(["subscription"]))
async def subscription_command(_: Client, message: Message):
    text = gen_subs_msg(message.from_user.id)
    await message.reply(text, reply_markup=gen_back_button(), quote=True, )


@Client.on_message(filters.incoming & filters.private &
                   filters.command(["subscribe"]))
async def sub_command(_: Client, message: Message):
    if len(message.command) == 1:
        await message.reply(sub_help_msg, reply_markup=gen_back_button(), quote=True)
    else:
        data = " ".join(message.command[1:])
        module = from_keyword_to_v(data)
        if module:
            success = add_to_subs(message.from_user.id, module)
            if success:
                await message.reply(sub_msg.format(module.name), quote=True)
            else:
                await message.reply(already_sub_msg.format(module.name), quote=True)
        else:
            await message.reply(not_found_msg.format(data), quote=True)


@Client.on_message(filters.incoming & filters.private &
                   filters.command(["unsubscribe"]))
async def un_sub_command(_: Client, message: Message):
    if len(message.command) == 1:
        await message.reply(unsub_help_msg, reply_markup=gen_back_button(), quote=True)
    else:
        data = " ".join(message.command[1:])
        module = from_keyword_to_v(data)
        if module:
            success = remove_from_subs(message.from_user.id, module)
            if success:
                await message.reply(unsub_msg.format(module.name), quote=True)
            else:
                await message.reply(not_sub_msg.format(module.name), quote=True)
        else:
            await message.reply(not_found_msg.format(data), quote=True)
