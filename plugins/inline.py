from pyrogram import Client, emoji
from pyrogram.types import InlineQuery, InputTextMessageContent, InlineQueryResultArticle

from defs.source import vtubers_info
from plugins.info import vtuber_msg, gen_info_button


@Client.on_inline_query()
async def inline_process(_: Client, query: InlineQuery):
    data = []
    text = query.query.split()
    nums = 0
    if not vtubers_info:
        return
    data_ = vtubers_info
    for key, v in data_.items():
        if len(text) == 0:
            data.append(InlineQueryResultArticle(
                v.name,
                InputTextMessageContent(vtuber_msg.format(
                    v.name,
                    v.follower,
                    v.timeStr,
                    v.notice,
                )),
                reply_markup=gen_info_button(v),
            ))
            nums += 1
        else:
            name = str(v.mid) + v.name + str(v.room_id)
            skip = False
            for i in text:
                if i not in name:
                    skip = True
            if not skip:
                data.append(InlineQueryResultArticle(
                    v.name,
                    InputTextMessageContent(vtuber_msg.format(
                        v.name,
                        v.follower,
                        v.timeStr,
                        v.notice,
                    )),
                    reply_markup=gen_info_button(v),
                ))
                nums += 1
        if nums >= 50:
            break
    if nums == 0:
        return await query.answer(
            results=[],
            switch_pm_text=f'{emoji.CROSS_MARK} 字符串 "{" ".join(text)}" 没有搜索到任何结果',
            switch_pm_parameter="help",
        )
    await query.answer(data,
                       switch_pm_text=f'{emoji.KEY} 搜索了 {len(vtubers_info.values())} 个 Vtuber',
                       switch_pm_parameter="help", )
