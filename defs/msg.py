from pyrogram.types import InlineKeyboardMarkup, InlineKeyboardButton

from defs.utils import Vtuber, TrackMessage
from defs.thumbnail import thumb
from ci import me

template = """
{}

<b>{}</b> 正在直播

<b>标题：</b><code>{}</code>
<b>人气值：</b><code>{}</code>
<b>开播时间：</b><code>{}</code>

@DD_YTbs_Live_Tracker | @DD_YTbs_Bot
"""


def gen_button(data: Vtuber) -> InlineKeyboardMarkup:
    data_ = [[InlineKeyboardButton("🔗️ 观看", url=data.room_link)],
             [InlineKeyboardButton("主页", url=data.space_link),
              InlineKeyboardButton(
                  "订阅",
                  url=f"https://t.me/{me.username}?start={data.room_id}"),
              InlineKeyboardButton(
                  "分享",
                  url=f"https://t.me/{me.username}?start=info-{data.room_id}"), ]
             ]
    return InlineKeyboardMarkup(data_)


def format_text(text: str) -> str:
    text = text.strip()
    for i in ["/", " ", "-", "@", "（", ]:
        text = text.replace(i, "_")
    for i in ["【", "】", "[", "]", "！", "(", ")", "`", "!", ]:
        text = text.replace(i, "")
    return text.strip()


def gen_tags(data: Vtuber) -> str:
    return f"#id{data.mid} #{format_text(data.name.split()[0])} "


async def gen_update_msg(data: Vtuber) -> TrackMessage:
    text = template.format(gen_tags(data), data.name, data.title, data.online,
                           data.liveStartTimeStr, )
    button = gen_button(data)
    img = await thumb(data.face, data.title, data.name)
    return TrackMessage(text, button, img)
