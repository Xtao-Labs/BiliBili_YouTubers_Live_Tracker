from typing import Optional
from defs.format_time import strf_time


class Vtuber:
    def __init__(self, data: dict):
        self.name: str = data["uname"]
        self.mid: int = data["mid"]
        self.space_link: str = f"https://space.bilibili.com/{self.mid}"
        self.title: str = data["title"]
        self.room_id: str = data["roomid"]
        self.room_link: str = f"https://live.bilibili.com/{self.room_id}"
        self.face: str = data["face"]
        self.follower: int = data["follower"]
        self.liveStatus: bool = data["liveStatus"]
        self.online: int = data["online"] if data["online"] else 0
        self.notice: str = data["notice"].replace(r"\n", "\n")
        self.time: int = data["time"]
        self.timeStr: str = strf_time(self.time)
        self.liveStartTime: int = data["liveStartTime"]
        self.liveStartTimeStr: str = strf_time(self.liveStartTime)


class TrackMessage:
    def __init__(self, text, button, img=None):
        self.text = text
        self.button = button
        self.img = img
