from os import sep
from os.path import exists
from shutil import copyfile
from typing import List, Optional

from ci import client, sqlite
from json import load
from defs.format_time import now_time
from defs.utils import Vtuber

vtubers_info: dict = {}
new_vtubers: List[int] = []
old_vtubers: List[int] = []
if exists(f"data{sep}info.json"):
    with open(f"data{sep}info.json", "r", encoding="utf-8") as file:
        temp_data = load(file)
    for temp in temp_data:
        temp_data_ = Vtuber(temp)
        vtubers_info[temp_data_.room_id] = temp_data_
if exists(f"data{sep}vtubers.json"):
    with open(f"data{sep}vtubers.json", "r", encoding="utf-8") as file:
        temp_data = load(file)
    new_vtubers = temp_data
if exists(f"data{sep}old_vtubers.json"):
    with open(f"data{sep}old_vtubers.json", "r", encoding="utf-8") as file:
        temp_data = load(file)
    old_vtubers = temp_data


async def update_data() -> None:
    global new_vtubers, old_vtubers
    if exists(f"data{sep}vtubers.json"):
        copyfile(f"data{sep}vtubers.json", f"data{sep}old_vtubers.json")
    data = await client.get("https://api.tokyo.vtbs.moe/v1/living")
    with open(f"data{sep}vtubers.json", "w", encoding="utf-8") as f:
        f.write(data.text)
    data = data.json()
    old_vtubers = new_vtubers
    new_vtubers = data
    sqlite["update_time"] = now_time()


async def update_info() -> None:
    global vtubers_info
    data = await client.get("https://api.tokyo.vtbs.moe/v1/fullInfo")
    with open(f"data{sep}info.json", "w", encoding="utf-8") as f:
        f.write(data.text)
    data = data.json()
    for i in data:
        data_ = Vtuber(i)
        vtubers_info[data_.room_id] = data_


def compare() -> List[Vtuber]:
    data = []
    for i in new_vtubers:
        if i not in old_vtubers:
            data.append(vtubers_info[i])
    return data


def from_name_to_v(name: str) -> Optional[Vtuber]:
    try:
        data = int(name)
    except ValueError:
        return None
    return vtubers_info.get(data, None)


def from_list_to_name(data: List) -> str:
    data_ = ""
    for i in data:
        v = vtubers_info.get(int(i), None)
        if isinstance(v, Vtuber):
            data_ += f"\n{v.name}"
    return data_


def from_keyword_to_v(keyword: str) -> Optional[Vtuber]:
    for value in vtubers_info.values():
        data = str(value.mid) + value.name + str(value.room_id)
        if keyword in data:
            return value
    return None
