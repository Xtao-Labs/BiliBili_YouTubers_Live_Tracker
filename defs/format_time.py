import pytz
from datetime import datetime, timedelta

tz = pytz.timezone("Asia/Shanghai")
date_format = "%Y/%m/%d %H:%M:%S"


def strf_time(data: int) -> str:
    # data = 1648111686000
    ts = datetime.fromtimestamp(data/1000, tz)
    return ts.strftime(date_format)


def now_time() -> str:
    # UTC
    ts = datetime.utcnow()
    # UTC+8
    ts = ts + timedelta(hours=8)
    return ts.strftime(date_format)
